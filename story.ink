# title: Quand les continents sombrèrent
# author: Nathanaël Marion

/* Après le nom de chaque passage est écrit le nombre de mots qu’il contient.

Nombre total de mots :
— Selon mon comptage, 496 mots.
— Selon Antidote, 499 mots. */

-> titre

=== titre === // 0 mots. C’est de l’interface, pas de l’histoire, voyons ! :p

*	[Commencer]
	-> intro
*	[À propos]
	-> a_propos

=== intro === // 27 mots.

« Grand-mère, raconte-moi quand…

— Quand les continents sombrèrent ? C’est une longue histoire…

— Allez !

— Bon. J’étais une fillette, comme toi. Assise sur la falaise, je contemplais…

-> options

=== options === // 15 mots.

*	… la jungle en contrebas. <>
	-> jungle
*	… le ciel. <>
	-> ciel
*	… l’océan au loin. <>
	-> ocean
*	{CHOICE_COUNT() == 0} … “Il est tard ! Au dodo !”
	-> dormir

=== jungle === // 33 mots.

Du moins, ce qui en restait. Incendies et séismes des dernières années, causés par le Démon, l’avaient détruite. Notre belle forêt ! Ce spectacle était si désolant ! Je préférai détourner les yeux vers…

-> options

=== ciel === // 34 mots.

Des nuages en colère crachaient leurs éclairs. La Déesse faisait preuve d’une rage que nous ne lui connaissions pas.

— Pour battre le méchant Démon, hein ?

—  C’est bien cela ! Mon regard se porta ensuite sur…

-> options

=== ocean === // 57 mots.

De moins en moins de terres pointaient à l’horizon et une partie de notre propre pays avait été dévorée par les flots. Étions-nous condamnés à disparaître sous les vagues ?

— Ben non ! Nous sommes toujours là.

— C’est vrai, nous sommes toujours là. Pour combien de temps, cependant ? Mais je m’égare. Je me tournai vers…

-> options

=== dormir === // 56 mots.

— Quoi, déjà ?

— Mais non ! C’est ce que mon père me dit à ce moment. Je rentrai donc pour dormir, tant bien que mal malgré les éléments qui se déchaînaient dehors. Le lendemain, je fus la première à me lever. Tout était calme.

— Le combat était terminé ?

— Écoute et tu sauras. Je…

*	… sortis. <>
	-> sortir
*	… réveillai mes parents. <>
	-> parents

=== sortir === // 69 mots

Silencieusement, je me glissai à l’extérieur et retournai à la falaise. La jungle avait disparu ; les terres sur l’horizon aussi. Il n’y avait que l’océan. Mais le ciel était si limpide !

— La Déesse a vaincu le Démon !

— C’est la première chose à laquelle je pensai. Folle de joie, je rentrai réveiller les villageois et nous nous rendîmes tous au temple.

— Vous vous sentiez comment ?

— N<>

-> fin

=== parents === // 29 mots

Ils nous dirent, à mes frères et à moi, de rester à la maison, et se précipitèrent à l’extérieur ; …

*	… j’obéis. <>
	-> obeir
*	… je désobéis. <>
	-> desobeir

=== obeir === // 39 mots.

Nous ne tenions pas en place. Que se passait-il ? Enfin, mon père rentra, criant : “L’océan a monté, mais elle a vaincu !”. Pendant que ma mère annonçait la nouvelle aux villageois, nous nous rendîmes au temple.

— Vous deviez être vraiment contents, pas vrai ?

— N<>

-> fin

=== desobeir === // 65 mots.

J’ordonnai à mes frères de rester sages et je sortis. Il n’y avait pas un souffle ; le soleil brillait. Sur le chemin de la falaise, je croisai mes parents qui rentraient.

— Ils t’ont réprimandée ?

— Non ! Au prix d’énormes sacrifices, la Déesse avait gagné. Nous récupérâmes mes frères et nous nous rendîmes au temple.

— Pour faire la fête !

— Pas encore, même si n<>

-> fin

=== fin === // 70 mots.

/* Il n’y a pas la première lettre car le début peut changer en fonction du passage précédent. */

ous étions heureux. Beaucoup avait été perdu — les continents avaient sombré ce jour-là —, mais tout était fini. Peut-être pour cent mille ans seulement, comme le disait le prêtre, peut-être pour toujours. Quelle importance ?

— Moi je dis que le Démon il a été vaincu pour toujours !

— Tu as raison, cela fait une meilleure fin pour l’histoire.

— Elle est finie ?

— Au moins pour ce soir. Allez, bonne nuit !

— Bonne nuit... »

-> menu_final

=== menu_final === // 0 mots. Ça aussi c’est de l’interface !

*	[Recommencer] # RESTART
	-> END

=== a_propos === // 0 mots. Ça, c’est du méta, bien évidemment.

Cette petite fiction interactive a été créée pour <a href="https:\/\/itch.io/jam/fr-partim-500-an-2019" target="_blank">la Partim 500, an 2019</a>. Elle a été écrite avec <a href="https:\/\/www.inklestudios.com/ink/" target="_blank">ink</a>.

Vous pouvez retrouver les autres fictions interactives que j’ai écrites <a href="https:\/\/ulukos.itch.io/" target="_blank">sur ma page itch.io</a>.

Merci d’avoir lu !

*	[Revenir] # RESTART
	-> END
